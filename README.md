# Stefan's build of st - the simple (suckless) terminal for FreeBSD

st is a minimalistic X terminal.

It consists of a single binary, configuration is done at compile-time by a
config.h file.

